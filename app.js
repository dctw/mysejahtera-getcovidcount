/*
Written by David Chin (https://davidch.in)

One time Installation
- Install latest nodeJS (this was developed on v16.2.0)
- run 'npm install'

To use the script, run the following

node app.js <lat> <long>

Example : 
node app.js 3.127404 101.665499
*/

const request = require('request');
const uuid = require('uuid')
const base64 = require('base-64');


function getDate(ts) {
    let date_ob = new Date(ts);
    let date = ("0" + (date_ob.getDate() + 1)).slice(-2)
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2)
    let year = date_ob.getFullYear();

    return `${year}-${month}-${date}`
}

function getTime(ts) {
    let date_ob = new Date(ts);
    let hours = ("0" + date_ob.getHours()).slice(-2)    
    let minutes = ("0" + date_ob.getMinutes()).slice(-2)  
    let seconds = ("0" + date_ob.getSeconds()).slice(-2)

    return hours + ":" + minutes + ":" + seconds
}

function getAuthToken() {
    return "Basic " + base64.encode(`${uuid.v1()}:${uuid.v1()}`)
}

const lat = process.argv[2]
const long = process.argv[3]

//console.log(`DEBUG: lat ${lat}, long ${long}`)

const URL = 'https://mysejahtera.malaysia.gov.my/register/api/nearby/hotspots?type=locateme'

const latlongBody = [
    {
        "classification": "LOW_RISK_NS",
        "lat": lat,
        "lng": long
    }
]

const options = {
    url: URL,
    json: true,
    body: latlongBody,
    headers: {
      'authorization': getAuthToken(),
      'content-type': 'application/json',
      'user-agent': 'MySejahtera/1.0.39 (iPhone; iOS 14.5.1; Scale/3.00)'
    },
    timeout: 5000
}

request.post(options, function(err, httpResponse, body) {

    if (!err) {
        const now = Date.now()
        const output = body.messages.en_US
        const count = output.split(' ').slice(5,6)
        console.log(JSON.stringify({
            "date": getDate(now),
            "time": getTime(now),
            "count": count[0]
        }))
    }

})
