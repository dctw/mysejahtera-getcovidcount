
Written by David Chin (https://davidch.in)

**One time Installation**
- Install latest nodeJS (this was developed on v16.2.0)
- run 'npm install'

To use the script, run the following :

    node app.js <lat> <long>

*Example :*  
`node app.js 3.127404 101.665499`

*Sample Output :* 
`RESULT: 25 case(s) within 1km radius from 3.127404,101.665499`
